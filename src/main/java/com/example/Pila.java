package com.example;

import java.util.ArrayList;
import java.util.List;


public class Pila {

    private List<String> guardados;
    private int sizePila;
    private boolean vacio;
    private int finalSize;

    Pila(int finalSize){
        guardados=new ArrayList<>();
        vacio=true;
        this.finalSize=finalSize;
        this.sizePila=0;
    }

    public boolean esVacia() {
            return vacio;
    }

    public String  pushData(String data) {
        if (sizePila<finalSize){
            guardados.add(sizePila,data);
            sizePila++;
            vacio=false;
            return "dato guardada";
        }
        return "pila llena";

    }

    public int getSize() {
        return sizePila;
    }

    public String popData() {
        if (sizePila==0)
            return "Error Pila Vacia";
        sizePila--;
        return guardados.remove(sizePila);
    }


}
