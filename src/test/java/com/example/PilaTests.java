package com.example;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PilaTests {

    private Pila pila;

    @Before
    public void setup()throws Exception{
        pila=new Pila(4);
    }


    @Test
    public void PilaEmpty ()throws Exception{
        assertEquals(true,pila.esVacia());
    }

    @Test
    public void TestAddElementAndCheckPilaIsEmpty()throws Exception{
        pila.pushData("1er dato");
        assertEquals(false,pila.esVacia());
    }

    @Test
    public void TestNewPilaSizeIsCero()throws Exception{
        assertEquals(0,pila.getSize());
    }

    @Test
    public void TestAddOneElementAndCheckSize()throws Exception{
        pila.pushData("new data");
        assertEquals(1,pila.getSize());
    }

    @Test
    public void TestAddThreeElementAndCheckSize()throws Exception{
        pila.pushData("data 1");
        pila.pushData("data 2");
        pila.pushData("data 3");
        assertEquals(3,pila.getSize());
    }

    @Test
    public void TestAddAndRemoveOneElementAndCheckSize()throws Exception{
        pila.pushData("new data");
        pila.popData();
        assertEquals(0,pila.getSize());
    }

    @Test
    public void TestCheckElementPushAndPop()throws Exception{
        pila.pushData("dato guardado");
        assertEquals("dato guardado",pila.popData());
    }

    @Test
    public void TestCheckElementPushAndPopTwoElement()throws Exception{
        pila.pushData("dato1 guardado");
        pila.pushData("dato2 guardado");
        assertEquals("dato2 guardado",pila.popData());
        assertEquals("dato1 guardado",pila.popData());
    }

    @Test
    public void TestDoPopToEmptyPila()throws Exception{
        assertEquals("Error Pila Vacia",pila.popData());
    }

    @Test
    public void TestPilaSizeisFull()throws Exception{

       assertEquals("dato guardada",pila.pushData("dato 1"));
       assertEquals("dato guardada",pila.pushData("dato 1"));
       assertEquals("dato guardada",pila.pushData("dato 1"));
       assertEquals("dato guardada",pila.pushData("dato 1"));
       assertEquals("pila llena"   ,pila.pushData("dato 1"));
    }

//-------------------------------------------------------
}
